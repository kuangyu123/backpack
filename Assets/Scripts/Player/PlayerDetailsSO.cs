﻿using UnityEngine;

namespace QFramework.Gun
{
    [CreateAssetMenu(fileName = "PlayerDetails_", menuName = "Scriptable Objects/Player Details")]
    public class PlayerDetailsSO : ScriptableObject
    {
        #region 标题 玩家基本信息
        [Space(10)]
        [Header("玩家基本信息")]
        #endregion
        #region 工具提示
        [Tooltip("玩家角色的名字。")]
        #endregion
        public string playerCharacterName;

        #region 工具提示
        [Tooltip("玩家的预制体游戏对象")]
        #endregion
        public GameObject playerPrefab;

        #region 工具提示
        [Tooltip("玩家的运行时动画控制器")]
        #endregion
        public RuntimeAnimatorController runtimeAnimatorController;

        #region 标题 生命值
        [Space(10)]
        [Header("生命值")]
        #endregion
        #region 工具提示
        [Tooltip("玩家的初始生命值数量")]
        #endregion
        public int playerHealthAmount;

        #region 标题 其他
        [Space(10)]
        [Header("其他")]
        #endregion
        #region 工具提示
        [Tooltip("用于小地图的玩家图标精灵")]
        #endregion
        public Sprite playerMiniMapIcon;

        #region 工具提示
        [Tooltip("玩家的手部精灵")]
        #endregion
        public Sprite playerHandSprite;

        #region 移动速度
        [Space(10)]
        [Header("移动速度")]
        #endregion
        [Tooltip("移动速度")]
        public float moveSpeed;

        #region 翻滚速度
        [Space(10)]
        [Header("翻滚速度")]
        #endregion
        [Tooltip("翻滚速度")]
        public float rollSpeed;

        #region 翻滚cd
        [Space(10)]
        [Header("翻滚cd")]
        #endregion
        [Tooltip("翻滚cd")]
        public float rollCooldown;

        #region 翻滚持续时间
        [Space(10)]
        [Header("翻滚持续时间")]
        #endregion
        [Tooltip("翻滚持续时间")]
        public float rollDuration;

        #region 验证
#if UNITY_EDITOR
        private void OnValidate()
        {
            CheckUtility.ValidateCheckEmptyString(this, nameof(playerCharacterName), playerCharacterName);
            CheckUtility.ValidateCheckNullValue(this, nameof(playerPrefab), playerPrefab);
            CheckUtility.ValidateCheckPositiveValue(this, nameof(playerHealthAmount), playerHealthAmount, false);
            CheckUtility.ValidateCheckNullValue(this, nameof(playerMiniMapIcon), playerMiniMapIcon);
            CheckUtility.ValidateCheckNullValue(this, nameof(playerHandSprite), playerHandSprite);
            CheckUtility.ValidateCheckNullValue(this, nameof(runtimeAnimatorController), runtimeAnimatorController);
        }
#endif
        #endregion
    }

}