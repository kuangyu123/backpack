﻿using UnityEngine;

namespace QFramework.Gun
{
    [CreateAssetMenu(fileName = "CurrentPlayer", menuName = "Scriptable Objects/Current Player")]
    public class CurrentPlayerSO : ScriptableObject
    {
        public PlayerDetailsSO playerDetails;
        public string playerName;

    }
}