﻿using UnityEngine;

namespace QFramework.Gun
{
    public class Health: MonoBehaviour
    {
        private int startingHealth;
        private int currentHealth;
        
        public void SetStartingHealth(int startingHealth)
        {
            this.startingHealth = startingHealth;
            currentHealth = startingHealth;
        }
        
        public int GetStartingHealth()
        {
            return startingHealth;
        }
        
        public int GetCurrentHealth()
        {
            return currentHealth;
        }
    }
}