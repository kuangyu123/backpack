
using System;
using System.Collections.Generic;
using UnityEngine;

namespace QFramework.Gun
{
    [DisallowMultipleComponent]
    public class GameManager : MonoSingleton<GameManager>
    {
        [SerializeField]private List<DungeonLevelSO> dungeonLevelList;
        [HideInInspector]public GameState currentGameState;
        [SerializeField]private int currentDungeonLevelIndex = 0;
        [SerializeField]private CurrentPlayerSO currentPlayer;
        
        private Room currentRoom;
        private Room previousRoom;
        private PlayerDetailsSO playerDetails;
        private Player player;

        public Material dimmedMaterial;
        public Material lightedMaterial;
        public Shader lightShader;
        private void Awake()
        {
            playerDetails = currentPlayer.playerDetails;
            InitPlayer();
            
        }

        private void Start()
        {
            currentGameState = GameState.gameStarted;
            
            //订阅房间改变事件
            EventCenter.Instance.AddEventListener<CurRoomChanged>(EventEnumType.current_room_changed,OnCurRoomChanged);
        }

        #region 事件相关

        
        private void OnDestroy()
        {
            EventCenter.Instance.RemoveEventListener<CurRoomChanged>(EventEnumType.current_room_changed,OnCurRoomChanged);
        }
        
        private void OnCurRoomChanged(CurRoomChanged arg)
        {
            if (arg.currentRoom != null && currentRoom!=arg.currentRoom)
            {
                currentRoom = arg.currentRoom;
                
                //TODO:创建敌人
            }
        }

        #endregion

        private void Update()
        {
            //测试按钮
            if (Input.GetKeyDown(KeyCode.R))
            {
                currentGameState = GameState.gameStarted;
                //GameObject obj = PoolMgr.Instance.GetObj("Circle");
            }
            HandleGameState();
        }

        #region 玩家相关

        private void InitPlayer()
        {
            player = playerDetails.playerPrefab.Instantiate().GetComponent<Player>();
            player.Initialize(playerDetails);
        }
        
        private void SetPlayerBirthPosition()
        {
            Vector3 playerPos = new Vector3(currentRoom.lowerBounds.x + currentRoom.upperBounds.x,
                currentRoom.lowerBounds.y + currentRoom.upperBounds.y, 0) / 2;
            playerPos = HelpUtility.GetSpawnNearestPosition(playerPos, currentRoom);
            player.transform.position = playerPos;  
        }
        
        public Player GetPlayer()
        {
            return player;
        }
        
        #endregion

        #region 房间相关

        public Room GetCurrentRoom()
        {
            return currentRoom;
        }

        public void SetCurrentRoom(Room room)
        {
            previousRoom = currentRoom;
            currentRoom = room;
            //触发当前房间变化事件
            EventCenter.Instance.EventTrigger(EventEnumType.current_room_changed, 
                new CurRoomChanged {currentRoom = room});
        }

        #endregion

        #region 游戏状态处理

        private void HandleGameState()
        {
            switch (currentGameState)
            {
                case GameState.gameStarted:
                    PlayDungeonLevel(currentDungeonLevelIndex);
                    currentGameState = GameState.playingLevel;
                    //打开游戏面板
                    UIKit.OpenPanel<UIGamePlay>();
                    break;
                case GameState.playingLevel:
                    break;
                case GameState.engagingEnemies:
                    break;
                case GameState.bossStage:
                    break;
                case GameState.engagingBoss:
                    break;
                case GameState.levelCompleted:
                    break;
                case GameState.gameWon:
                    break;
                case GameState.gameLost:
                    break;
                case GameState.gamePaused:
                    break;
                case GameState.dungeonOverviewMap:
                    break;
                case GameState.restartGame:
                    break;
            }
        }

        private void PlayDungeonLevel(int dungeonLevelIndex)
        {
            bool dungeonGenerated = DungeonBuilder.Instance.GenerateDungeon(dungeonLevelList[dungeonLevelIndex]);
            if (!dungeonGenerated)
            {
                Debug.Log("Dungeon generation failed!");
            }
            //设置玩家初始位置
            SetPlayerBirthPosition();
        }
        

        #endregion

        private void OnValidate()
        {
            
#if UNITY_EDITOR
            CheckUtility.ValidateCheckEnumerableValues(this, nameof(dungeonLevelList), dungeonLevelList);
#endif
        }
    }
    
}

