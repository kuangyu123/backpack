﻿using System.Collections.Generic;
using QFramework.NodeGraph;
using UnityEditor;
using UnityEngine;

namespace QFramework.Gun
{
    
    [CreateAssetMenu(fileName = "Room_", menuName = "Scriptable Objects/Room")]
    public class RoomTemplateSO : ScriptableObject
    {
        [HideInInspector] public string guid;

        #region Header ROOM PREFAB

        [Space(10)]
        [Header("ROOM PREFAB")]

        #endregion Header ROOM PREFAB

        #region Tooltip

        [Tooltip("房间的游戏对象预制件（此预制件将包含房间的所有瓷砖地图和环境游戏对象）")]

        #endregion Tooltip

        public GameObject prefab;

        [HideInInspector] public GameObject previousPrefab; // 用于在复制脚本对象（SO）并更改预制件时重新生成GUID

        #region Header ROOM CONFIGURATION

        [Space(10)]
        [Header("房间配置")]

        #endregion Header ROOM CONFIGURATION

        #region Tooltip

        [Tooltip("房间节点类型的脚本对象（SO）。房间节点类型与房间节点图中使用的房间节点对应。例外情况是走廊。在房间节点图中，只有一个走廊类型 'Corridor'。对于房间模板，则有两种走廊节点类型 - 'CorridorNS'（南北走廊）和 'CorridorEW'（东西走廊）。")]

        #endregion Tooltip

        public RoomNodeTypeSO roomNodeType;

        #region Tooltip

        [Tooltip("如果想象一个完全包含房间瓷砖地图的矩形，房间的下边界表示该矩形左下角的坐标。这应该从房间的瓷砖地图中确定（使用坐标刷指针获取矩形左下角在瓷砖地图中的网格位置）（注意：这是本地瓷砖地图坐标，而不是世界坐标）")]

        #endregion Tooltip

        public Vector2Int lowerBounds;

        #region Tooltip

        [Tooltip("如果想象一个完全包含房间瓷砖地图的矩形，房间的上边界表示该矩形右上角的坐标。这应该从房间的瓷砖地图中确定（使用坐标刷指针获取矩形右上角在瓷砖地图中的网格位置）（注意：这是本地瓷砖地图坐标，而不是世界坐标）")]

        #endregion Tooltip

        public Vector2Int upperBounds;

        #region Tooltip

        [Tooltip("一个房间最多应有四个门口 - 分别位于四个方向。它们应具有一致的3个瓷砖宽度开口，中间的瓷砖位置为门口的坐标位置。")]

        #endregion Tooltip

        [SerializeField] public List<Doorway> doorwayList;

        #region Tooltip

        [Tooltip("房间中所有可能的生成位置（用于敌人和宝箱）应以瓷砖地图坐标的形式添加到此数组中")]

        #endregion Tooltip

        public Vector2Int[] spawnPositionArray;



        /// <summary>
        /// Returns the list of Entrances for the room template
        /// </summary>
        public List<Doorway> GetDoorwayList()
        {
            return doorwayList;
        }

        #region Validation

    #if UNITY_EDITOR

        // Validate SO fields
        private void OnValidate()
        {
            // Set unique GUID if empty or the prefab changes
            if (guid == "" || previousPrefab != prefab)
            {
                guid = GUID.Generate().ToString();
                previousPrefab = prefab;
                EditorUtility.SetDirty(this);
            }

            CheckUtility.ValidateCheckEnumerableValues(this, nameof(doorwayList), doorwayList);

            // Check spawn positions populated
            CheckUtility.ValidateCheckEnumerableValues(this, nameof(spawnPositionArray), spawnPositionArray);
        }

    #endif

        #endregion Validation

    }
}
