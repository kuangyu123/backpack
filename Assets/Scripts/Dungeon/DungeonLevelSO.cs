using System.Collections.Generic;
using QFramework.NodeGraph;
using UnityEngine;

namespace QFramework.Gun
{
    
    // 创建一个ScriptableObject类型的类，用于管理地下城关卡的配置数据
    [CreateAssetMenu(fileName = "DungeonLevel_", menuName = "Scriptable Objects/Dungeon Level")]
    public class DungeonLevelSO : ScriptableObject
    {
            #region Header BASIC LEVEL DETAILS

            [Space(10)]
            [Header("基础关卡详情")]

            #endregion Header BASIC LEVEL DETAILS

            #region Tooltip

            [Tooltip("关卡的名称")]

            #endregion Tooltip

            public string levelName;  // 关卡名称

            #region Header ROOM TEMPLATES FOR LEVEL

            [Space(10)]
            [Header("关卡中的房间模板")]

            #endregion Header ROOM TEMPLATES FOR LEVEL

            #region Tooltip

            [Tooltip("将希望包含在关卡中的房间模板填充到列表中。需要确保房间模板包含了在关卡的房间节点图中指定的所有房间节点类型。")]

            #endregion Tooltip

            public List<RoomTemplateSO> roomTemplateList;  // 关卡中的房间模板列表

            #region Header ROOM NODE GRAPHS FOR LEVEL

            [Space(10)]
            [Header("关卡中的房间节点图")]

            #endregion Header ROOM NODE GRAPHS FOR LEVEL

            #region Tooltip

            [Tooltip("将要在关卡中随机选择的房间节点图填充到列表中。")]

            #endregion Tooltip

            public List<RoomNodeGraphSO> roomNodeGraphList;  // 关卡中的房间节点图列表

            #region Validation

        #if UNITY_EDITOR

            // 验证脚本对象中输入的详细信息
            private void OnValidate()
            {
                // 检查关卡名称是否为空
                CheckUtility.ValidateCheckEmptyString(this, nameof(levelName), levelName);

                // 检查房间模板列表和房间节点图列表是否有值
                if (CheckUtility.ValidateCheckEnumerableValues(this, nameof(roomTemplateList), roomTemplateList))
                    return;
                if (CheckUtility.ValidateCheckEnumerableValues(this, nameof(roomNodeGraphList), roomNodeGraphList))
                    return;

                // 检查指定的节点图中是否为所有节点类型指定了房间模板

                // 首先检查南北走廊、东西走廊和入口类型是否已指定
                bool isEWCorridor = false;
                bool isNSCorridor = false;
                bool isEntrance = false;

                // 遍历所有房间模板，检查这些节点类型是否已被指定
                foreach (RoomTemplateSO roomTemplateSO in roomTemplateList)
                {
                    if (roomTemplateSO == null)
                        return;

                    if (roomTemplateSO.roomNodeType.isCorridorEW)
                        isEWCorridor = true;

                    if (roomTemplateSO.roomNodeType.isCorridorNS)
                        isNSCorridor = true;

                    if (roomTemplateSO.roomNodeType.isEntrance)
                        isEntrance = true;
                }

                // 如果没有指定东西走廊类型，则提示警告
                if (isEWCorridor == false)
                {
                    Debug.Log("在 " + this.name.ToString() + " 中 : 未指定东西走廊类型的房间模板");
                }

                // 如果没有指定南北走廊类型，则提示警告
                if (isNSCorridor == false)
                {
                    Debug.Log("在 " + this.name.ToString() + " 中 : 未指定南北走廊类型的房间模板");
                }

                // 如果没有指定入口类型，则提示警告
                if (isEntrance == false)
                {
                    Debug.Log("在 " + this.name.ToString() + " 中 : 未指定入口类型的房间模板");
                }

                // 遍历所有节点图
                foreach (RoomNodeGraphSO roomNodeGraph in roomNodeGraphList)
                {
                    if (roomNodeGraph == null)
                        return;

                    // 遍历节点图中的所有节点
                    foreach (RoomNodeSO roomNodeSO in roomNodeGraph.roomNodeList)
                    {
                        if (roomNodeSO == null)
                            continue;

                        // 检查是否为每个房间节点类型指定了房间模板（已检查过走廊和入口类型）
                        if (roomNodeSO.roomNodeType.isEntrance || roomNodeSO.roomNodeType.isCorridorEW || roomNodeSO.roomNodeType.isCorridorNS || roomNodeSO.roomNodeType.isCorridor || roomNodeSO.roomNodeType.isNone)
                            continue;

                        bool isRoomNodeTypeFound = false;

                        // 遍历所有房间模板，检查是否为该节点类型指定了模板
                        foreach (RoomTemplateSO roomTemplateSO in roomTemplateList)
                        {
                            if (roomTemplateSO == null)
                                continue;

                            if (roomTemplateSO.roomNodeType == roomNodeSO.roomNodeType)
                            {
                                isRoomNodeTypeFound = true;
                                break;
                            }
                        }

                        // 如果未找到对应的房间模板，提示警告
                        if (!isRoomNodeTypeFound)
                            Debug.Log("在 " + this.name.ToString() + " 中 : 节点图 " + roomNodeGraph.name.ToString() + " 中未找到 " + roomNodeSO.roomNodeType.name.ToString() + " 类型的房间模板");
                    }
                }
            }

        #endif

            #endregion Validation
        }
}