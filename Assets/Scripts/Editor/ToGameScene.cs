using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine;

namespace QFramework.Monk
{
    public class ToGameScene 
    {
        [MenuItem("Tools/Open GameScene", false, 0)]
        static void OpenGameScene()
        {
            string scenePath = "Assets/Scenes/GameScene.unity"; // 请根据实际情况修改路径
            EditorSceneManager.OpenScene(scenePath);
        }
    }
}

