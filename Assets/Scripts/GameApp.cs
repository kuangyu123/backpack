using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace QFramework.Gun
{
    public class GameApp : Architecture<GameApp>
    {
        protected override void Init()
        {
            Application.targetFrameRate = 60;
            ResKit.Init();
            
        }
    }
    
}
