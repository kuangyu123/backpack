﻿using UnityEngine;

namespace QFramework.Gun
{
    public static class HelpUtility
    {
        private static Camera mainCamera;
        /// <summary>
        /// 获取最近的出生点
        /// </summary>
        /// <param name="playerPos"></param>
        /// <param name="currentRoom"></param>
        /// <returns></returns>
        public static Vector3 GetSpawnNearestPosition(Vector3 playerPos, Room currentRoom)
        {
            var grid = currentRoom.instantiatedRoom.grid;
            
            Vector3 nearestPosition = new Vector3(1000f, 1000f, 0);

            foreach (var spawnPosition in currentRoom.spawnPositionArray)
            {
                //得到找个瓦片的世界坐标
                var tileWorldPosition = grid.CellToWorld((Vector3Int)spawnPosition);
                //比较距离
                if (Vector3.Distance(playerPos, tileWorldPosition) < Vector3.Distance(playerPos, nearestPosition))
                {
                    nearestPosition = tileWorldPosition;
                }
                
            }
            
            return nearestPosition;
        }

        /// <summary>
        /// 获得xy方向的角度
        /// </summary>
        /// <param name="dir"></param>
        /// <returns></returns>
        public static float GetAngleByDir(Vector3 dir)
        {
            return Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg;
        }

        /// <summary>
        /// 获得鼠标世界坐标
        /// </summary>
        /// <returns></returns>
        public static Vector3 GetMouseWorldPosition()
        {
            if (mainCamera==null)
            {
                mainCamera = Camera.main;
            }
            //不得超出屏幕
            Vector3 mousePosOnScreen = Input.mousePosition;
            mousePosOnScreen.x = Mathf.Clamp(mousePosOnScreen.x, 0, Screen.width);
            mousePosOnScreen.y = Mathf.Clamp(mousePosOnScreen.y, 0, Screen.height);
            Vector3 mousePos = mainCamera.ScreenToWorldPoint(mousePosOnScreen);
            mousePos.z = 0;
            return mousePos;
        }
        
        /// <summary>
        /// 通过角度获得方向
        /// </summary>
        public static AimDirection GetAimDirection(float angleDegrees)
        {
            AimDirection aimDirection;

            // Set player direction
            //Up Right
            if (angleDegrees >= 22f && angleDegrees <= 67f)
            {
                aimDirection = AimDirection.UpRight;
            }
            // Up
            else if (angleDegrees > 67f && angleDegrees <= 112f)
            {
                aimDirection = AimDirection.Up;
            }
            // Up Left
            else if (angleDegrees > 112f && angleDegrees <= 158f)
            {
                aimDirection = AimDirection.UpLeft;
            }
            // Left
            else if ((angleDegrees <= 180f && angleDegrees > 158f) || (angleDegrees > -180 && angleDegrees <= -135f))
            {
                aimDirection = AimDirection.Left;
            }
            // Down
            else if ((angleDegrees > -135f && angleDegrees <= -45f))
            {
                aimDirection = AimDirection.Down;
            }
            // Right
            else if ((angleDegrees > -45f && angleDegrees <= 0f) || (angleDegrees > 0 && angleDegrees < 22f))
            {
                aimDirection = AimDirection.Right;
            }
            else
            {
                aimDirection = AimDirection.Right;
            }

            return aimDirection;

        }
    }
}