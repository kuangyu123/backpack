using System;
using System.Collections;
using System.Collections.Generic;
using Cinemachine;
using QFramework.Gun;
using UnityEngine;

[RequireComponent(typeof(CinemachineTargetGroup))]
public class CameraTarget : MonoBehaviour
{
    protected CinemachineTargetGroup targetGroup;

    public Transform cursorTarget;
    private void Awake()
    {
        targetGroup = GetComponent<CinemachineTargetGroup>();
    }

    void Start()
    {
        SetCinemachineTarget();
    }

    private void SetCinemachineTarget()
    {
        CinemachineTargetGroup.Target target1 = new CinemachineTargetGroup.Target()
        {
            target = GameManager.Instance.GetPlayer().transform,
            weight = 2f,
            radius = 1f
        };
        CinemachineTargetGroup.Target target2 = new CinemachineTargetGroup.Target()
        {
            target = cursorTarget,
            weight = 1f,
            radius = 1f
        };
        CinemachineTargetGroup.Target[] targets = new CinemachineTargetGroup.Target[] { target1 , target2};
        
        targetGroup.m_Targets = targets;
    }
    
    private void Update()
    {
        cursorTarget.position = HelpUtility.GetMouseWorldPosition();
    }
}
