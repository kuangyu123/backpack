using System;
using System.Collections.Generic;
using QFramework.Gun;
using UnityEditor;
using UnityEngine;
using UnityEditor.Callbacks;
namespace QFramework.NodeGraph
{
    public class RoomNodeGraphEditor : EditorWindow
    {
        
        private Vector2 graphOffset;
        private Vector2 graphDrag;
        
        private static RoomNodeGraphSO mRoomNodeGraphSO;
        private  RoomNodeTypeListSO mRoomNodeTypeListSO;
        private RoomNodeSO mCurrentRoomNode;
        
        private const float nodeHeight = 70;
        private const float nodeWidth = 160;
        private const int nodePadding = 25;
        private const int nodeBorder = 10;
        private const float lineWidth = 3;
        private const float connectingLineArrowSize = 5;
        private const float connectingLineWidth = 5;
        
        // Grid Spacing
        private const float gridLarge = 100f;
        private const float gridSmall = 25f;
        
        private GUIStyle mNodeNormalStyle;
        private GUIStyle mNodeSelectedStyle;
        [MenuItem("RoomNodeGraph",menuItem = "Tools/RoomNodeEditor")]
        private static void OpenWindow()
        {
             GetWindow<RoomNodeGraphEditor>("关卡编辑器");
        }

        /// <summary>
        /// 双击 打开的RoomNodeGraphSO文件
        /// </summary>
        /// <param name="instanceID"></param>
        /// <returns></returns>
        [OnOpenAsset(0)]
        public static bool OnDoubleClickAsset(int instanceID)
        {
            RoomNodeGraphSO roomNodeGraphSO = EditorUtility.InstanceIDToObject(instanceID) as RoomNodeGraphSO;
            if (roomNodeGraphSO != null)
            {
                mRoomNodeGraphSO = roomNodeGraphSO;
                OpenWindow();
                return true;
            }
            return false;
        }
        private void OnEnable()
        {
            Selection.selectionChanged += OnSelectionChanged;
            
            //设置node的样式
            mNodeNormalStyle = new GUIStyle();
            mNodeNormalStyle.normal.background = EditorGUIUtility.Load("node1") as Texture2D;
            mNodeNormalStyle.normal.textColor = Color.white;
            mNodeNormalStyle.alignment = TextAnchor.MiddleCenter;
            mNodeNormalStyle.padding = new RectOffset(nodePadding, nodePadding, nodePadding, nodePadding);
            mNodeNormalStyle.border = new RectOffset(nodeBorder, nodeBorder, nodeBorder, nodeBorder);
            
            mNodeSelectedStyle = new GUIStyle();
            mNodeSelectedStyle.normal.background = EditorGUIUtility.Load("node2 on") as Texture2D;
            mNodeSelectedStyle.normal.textColor = Color.white;
            mNodeSelectedStyle.alignment = TextAnchor.MiddleCenter;
            mNodeSelectedStyle.padding = new RectOffset(nodePadding, nodePadding, nodePadding, nodePadding);
            mNodeSelectedStyle.border = new RectOffset(nodeBorder, nodeBorder, nodeBorder, nodeBorder);
            
            //加载m_RoomNodeTypeListSO
            mRoomNodeTypeListSO = mRoomNodeGraphSO.roomNodeTypeList;
            mRoomNodeGraphSO.ClearSelectedNodes();
            //Debug.Log(m_RoomNodeTypeListSO==null);
        }

        private void OnDisable()
        {
            mRoomNodeTypeListSO = null;
            Selection.selectionChanged -= OnSelectionChanged;
        }

        private void OnSelectionChanged()
        {
            RoomNodeGraphSO roomNodeGraphSO = Selection.activeObject as RoomNodeGraphSO;
            if (roomNodeGraphSO != null)
            {
                mRoomNodeGraphSO = roomNodeGraphSO;
                GUI.changed = true;
            }
        }

        #region 菜单相关

        private void ShowCreateNodeMenu(Vector2 mousePosition)
        {
            GenericMenu menu = new  GenericMenu();
            foreach (var roomNodeType in mRoomNodeTypeListSO.roomNodeTypes)
            {
                if (roomNodeType.displayInNodeGraphEditor)
                {
                    menu.AddItem(new GUIContent(roomNodeType.roomNodeTypeName), false, () =>
                    {
                        CreateRoomNodeByType(mousePosition, roomNodeType);
                    });
                    
                }
            }
            //选择所有节点
            menu.AddSeparator("");
            menu.AddItem(new GUIContent("反选所有"), false, SelectAllNode);
            //删除
            menu.AddSeparator("");
            menu.AddItem(new GUIContent("删除选中/删除连线"), false, DeleteSelectedLinks);
            menu.AddItem(new GUIContent("删除选中/删除节点"), false, DeleteSelectedNodes);
            menu.ShowAsContext();
        }

        private void DeleteSelectedNodes()
        {
            Queue<string> selectedIds = new Queue<string>();
            foreach (var roomNodeSo in mRoomNodeGraphSO.roomNodeList)
            {
                if (roomNodeSo.isSelected&&!roomNodeSo.roomNodeType.isEntrance)
                {
                    roomNodeSo.ClearLinks();
                    selectedIds.Enqueue(roomNodeSo.id);
                }
            }
            mRoomNodeGraphSO.RemoveNodeByIds(selectedIds);
        }

        private void DeleteSelectedLinks()
        {
            foreach (var roomNodeSo in mRoomNodeGraphSO.roomNodeList)
            {
                if (roomNodeSo.isSelected)
                {
                    roomNodeSo.ClearLinks();
                }
            }
        }

        private void SelectAllNode()
        {
            foreach (var roomNodeSo in mRoomNodeGraphSO.roomNodeList)
            {
                roomNodeSo.isSelected = !roomNodeSo.isSelected;
            }
            GUI.changed = true;
        }

        private  void CreateRoomNodeByType(Vector2 mousePosition, RoomNodeTypeSO roomNodeType)
        {
            if (mRoomNodeGraphSO.roomNodeList.Count==0)
            {
                //添加入口节点
                CreateRoomNode(new Vector2(200,200), mRoomNodeTypeListSO.roomNodeTypes.Find(x=>x.isEntrance));
            }
            CreateRoomNode(mousePosition, roomNodeType);
        }

        private void CreateRoomNode(Vector2 mousePosition, RoomNodeTypeSO roomNodeType)
        {
            //创建RoomNodeSO
            RoomNodeSO roomNodeSO = ScriptableObject.CreateInstance<RoomNodeSO>();
            roomNodeSO.name = "RoomNode";
            roomNodeSO.Init(roomNodeType, new Rect(mousePosition.x, mousePosition.y, nodeWidth, nodeHeight),mRoomNodeGraphSO);
            //初始化节点
            //添加到RoomNodeGraphSO
            mRoomNodeGraphSO.AddRoomNode(roomNodeSO);
            
        }

        #endregion
        #region 事件与绘制
        private void OnGUI()
        {
            if (mRoomNodeGraphSO == null)
            {
                return;
            }
            
            DrawBackgroundGrid(gridSmall, 0.2f, Color.gray);
            DrawBackgroundGrid(gridLarge, 0.3f, Color.gray);
            
            ProgressGraphEvents(Event.current);
            
            DrawDragLine();
            DrawConnectionLines();
            DrawNodes();
            if (GUI.changed)
            {
                Repaint();
            }
            
        }

        private void ProgressGraphEvents(Event current)
        {
            graphDrag = Vector2.zero;
            //判断当前鼠标是否在node上
            if (mCurrentRoomNode==null||!mCurrentRoomNode.isLeftClickDragging)
            {
                mCurrentRoomNode = IsMouseOverNode(Event.current.mousePosition);
            }
            //当前node为null，或者需要绘制连线
            if (mCurrentRoomNode==null||mRoomNodeGraphSO.lineFRoomNodeSo!=null)
            {
                //画布的事件
                ProgressEvents(current);
            }
            else
            {
                //当前node的事件
                mCurrentRoomNode.ProgressEvents(Event.current);
            }
        }


        private RoomNodeSO IsMouseOverNode(Vector2 mousePosition)
        {
            foreach (var nodeSo in mRoomNodeGraphSO.roomNodeList)
            {
                if (nodeSo.mRect.Contains(mousePosition))
                {
                    return nodeSo;
                }
            }
            return null;
        }
    
        private void ProgressEvents(Event current)
        {
            switch (current.type)
            {
                case EventType.MouseDown:
                    ProgressMouseDown(current);
                    break;
                case EventType.MouseUp:
                    ProgressMouseUp(current);
                    break;
                case EventType.MouseDrag:
                    ProgressMouseDrag(current);
                    break;
            }
        }

        private void ProgressMouseDrag(Event current)
        {
            if (current.button == 1)
            {
                //绘制连线
                if (mRoomNodeGraphSO.lineFRoomNodeSo!=null)
                {
                    mRoomNodeGraphSO.lineStartPoint += current.delta;
                    
                    GUI.changed = true;
                }
            }else if (current.button == 0)
            {
                graphDrag = current.delta;

                for (int i = 0; i < mRoomNodeGraphSO.roomNodeList.Count; i++)
                {
                    mRoomNodeGraphSO.roomNodeList[i].DragNode(current.delta);
                }

                GUI.changed = true;
            }
        }

        private void ProgressMouseUp(Event current)
        {
            //右键
            if (current.button==1)
            {
                //子父节点链接
                if (mRoomNodeGraphSO.lineFRoomNodeSo != null)
                {
                    var childNode = IsMouseOverNode(current.mousePosition);
                    var parentNode = mRoomNodeGraphSO.lineFRoomNodeSo;
                    if (childNode!=null&&CheckCanLink(parentNode,childNode))
                    {
                        parentNode.AddChildNode(childNode);
                        childNode.AddParentNode(parentNode);
                    }
                    
                }
                //清除连线
                mRoomNodeGraphSO.ClearLineFrom(); 
               
            }
        }

        private bool CheckCanLink(RoomNodeSO parentNode, RoomNodeSO childNode)
        {
            //boss房间只能链接一个
            bool hasBossRoomLink = false;
            foreach (var roomNodeSo in mRoomNodeGraphSO.roomNodeList)
            {
                if (roomNodeSo.roomNodeType.isBossRoom&&roomNodeSo.parentRoomNodeIDList.Count>0)
                {
                    hasBossRoomLink = true;
                    break;
                }
            }

            if (hasBossRoomLink&&childNode.roomNodeType.isBossRoom)
            {
                Debug.Log("boss房间只能链接一个");
                return  false;
            }
            
            //自己不能和自己连接
            if (parentNode==childNode)
            {
                Debug.Log("自己不能和自己连接");
                return  false;
            }
            //父节点不能有子节点
            if (parentNode.childRoomNodeIDList.Contains(childNode.id)||parentNode.parentRoomNodeIDList.Contains(childNode.id))
            {
                Debug.Log("父节点不能有子节点");
                return  false;
            }
            //子节点不能有父节点
            if (childNode.parentRoomNodeIDList.Contains(parentNode.id)||childNode.childRoomNodeIDList.Contains(parentNode.id))
            {
                Debug.Log("子节点不能有父节点");
                return  false;
            }
            //不能链接入口
            if (childNode.roomNodeType.isEntrance)
            {
                Debug.Log("不能链接入口");
                return  false;
            }
            //走廊不能链接走廊
            if (parentNode.roomNodeType.isCorridor&&childNode.roomNodeType.isCorridor)
            {
                Debug.Log("走廊不能链接");
                return  false;
            }
            //只有一个父节点
            if (childNode.parentRoomNodeIDList.Count>0)
            {
                Debug.Log("只有一个父节点");
                return  false;
            }
            //房间不能链接房间
            if (!parentNode.roomNodeType.isCorridor&&!childNode.roomNodeType.isCorridor)
            {
                Debug.Log("房间不能链接房间");
                return  false;
            }
            //不能链接None
            if (parentNode.roomNodeType.isNone||childNode.roomNodeType.isNone)
            {
                Debug.Log("房间不能链接None");
                return  false;
            }
            //房间最多3个子节点
            if (!parentNode.roomNodeType.isCorridor&&parentNode.childRoomNodeIDList.Count>=3)
            {
                Debug.Log("房间最多3个子节点");
                return  false;
            }

            return true;
        }

        private void ProgressMouseDown(Event current)
        {
            if (current.button  == 1)
            {
                //展示创建菜单
                ShowCreateNodeMenu(current.mousePosition);
            }else if (current.button == 0)
            {
                //清除选择
                mRoomNodeGraphSO.ClearLineFrom();
                ClearSelectedNodes();
            }
        }

        private void ClearSelectedNodes()
        {
            foreach (var roomNodeSo in mRoomNodeGraphSO.roomNodeList)
            {
                roomNodeSo.isSelected = false;
            }
            GUI.changed = true;
        }
        

        private void DrawNodes()
        {
            foreach (var roomNodeSo in mRoomNodeGraphSO.roomNodeList)
            {
                if (roomNodeSo.isSelected)
                {
                    roomNodeSo.Draw(mNodeSelectedStyle);
                }
                else
                {
                    roomNodeSo.Draw(mNodeNormalStyle);
                    
                }
            }
        }

        private void DrawDragLine()
        {
            //绘制连线
            if (mRoomNodeGraphSO.lineFRoomNodeSo != null)
            {
                var startPoint = mRoomNodeGraphSO.lineFRoomNodeSo.mRect.center;
                var  endPoint = mRoomNodeGraphSO.lineStartPoint;
                Handles.DrawBezier(startPoint,endPoint,
                    startPoint,endPoint,Color.red,null,lineWidth);
                
            }
        }

        private void DrawConnectionLines()
        {
            foreach (var parentNode in mRoomNodeGraphSO.roomNodeList)
            {
                foreach (var childNodeId in parentNode.childRoomNodeIDList)
                {
                    if (!mRoomNodeGraphSO.roomNodeDictionary.ContainsKey(childNodeId)) continue;
                    var childNode = mRoomNodeGraphSO.roomNodeDictionary[childNodeId];
                    DrawLine(parentNode, childNode, Color.white);
                }
            }
        }

        private void DrawLine(RoomNodeSO parentRoomNode, RoomNodeSO childRoomNode,Color color)
        {
            // get line start and end position
            Vector2 startPosition = parentRoomNode.mRect.center;
            Vector2 endPosition = childRoomNode.mRect.center;

            // calculate midway point
            Vector2 midPosition = (endPosition + startPosition) / 2f;

            // Vector from start to end position of line
            Vector2 direction = endPosition - startPosition;

            // Calulate normalised perpendicular positions from the mid point
            Vector2 arrowTailPoint1 = midPosition - new Vector2(-direction.y, direction.x).normalized * connectingLineArrowSize;
            Vector2 arrowTailPoint2 = midPosition + new Vector2(-direction.y, direction.x).normalized * connectingLineArrowSize;

            // Calculate mid point offset position for arrow head
            Vector2 arrowHeadPoint = midPosition + direction.normalized * connectingLineArrowSize;

            // Draw Arrow
            Handles.DrawBezier(arrowHeadPoint, arrowTailPoint1, arrowHeadPoint, arrowTailPoint1, color, null, connectingLineWidth);
            Handles.DrawBezier(arrowHeadPoint, arrowTailPoint2, arrowHeadPoint, arrowTailPoint2, color, null, connectingLineWidth);

            // Draw line
            Handles.DrawBezier(startPosition, endPosition, startPosition, endPosition, color, null, connectingLineWidth);

            //GUI.changed = true;
        }
        
        private void DrawBackgroundGrid(float gridSize, float gridOpacity, Color gridColor)
        {
            int verticalLineCount = Mathf.CeilToInt((position.width + gridSize) / gridSize);
            int horizontalLineCount = Mathf.CeilToInt((position.height + gridSize) / gridSize);

            Handles.color = new Color(gridColor.r, gridColor.g, gridColor.b, gridOpacity);

            graphOffset += graphDrag * 0.5f;

            Vector3 gridOffset = new Vector3(graphOffset.x % gridSize, graphOffset.y % gridSize, 0);
            
            
            for (int i = 0; i < verticalLineCount; i++)
            {
                Handles.DrawLine(new Vector3(gridSize * i, -gridSize, 0) + gridOffset, new Vector3(gridSize * i, position.height + gridSize, 0f) + gridOffset);
            }

            for (int j = 0; j < horizontalLineCount; j++)
            {
                Handles.DrawLine(new Vector3(-gridSize, gridSize * j, 0) + gridOffset, new Vector3(position.width + gridSize, gridSize * j, 0f) + gridOffset);
            }

            Handles.color = Color.white;

        }
        
        #endregion
    }
    
}
