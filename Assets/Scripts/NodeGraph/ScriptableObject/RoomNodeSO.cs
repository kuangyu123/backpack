using System.Collections;
using System.Collections.Generic;
using System.Linq;
using QFramework.Gun;
using Unity.Collections;
using Unity.VisualScripting;
using UnityEditor;
using UnityEngine;

namespace QFramework.NodeGraph
{
    public class RoomNodeSO : ScriptableObject
    {
        [HideInInspector] public string id;
        [HideInInspector]public List<string> parentRoomNodeIDList = new List<string>();
        [HideInInspector]public List<string> childRoomNodeIDList = new List<string>();
        [HideInInspector] public RoomNodeGraphSO roomNodeGraph;
        public RoomNodeTypeSO roomNodeType;
        [HideInInspector] public RoomNodeTypeListSO roomNodeTypeList;
        [HideInInspector] public Rect mRect;
        
        public void Init(RoomNodeTypeSO roomNodeTypeSo, Rect rect
            , RoomNodeGraphSO roomNodeGraphSo)
        {
            id = GUID.Generate().ToString();
            
            roomNodeType = roomNodeTypeSo;
            mRect = rect;
            roomNodeGraph = roomNodeGraphSo;
            roomNodeTypeList =roomNodeGraphSo.roomNodeTypeList;
        }
        
#if UNITY_EDITOR
        [HideInInspector] public bool isSelected;
        [HideInInspector] public bool isLeftClickDragging;
        public void Draw(GUIStyle mStyle)
        {
            GUILayout.BeginArea(mRect, mStyle);
            
            EditorGUI.BeginChangeCheck();

            if (parentRoomNodeIDList.Count>0||roomNodeType.isEntrance)
            {
                EditorGUILayout.LabelField(roomNodeType.roomNodeTypeName);
            }
            else
            {
                
                int oldIndex = roomNodeTypeList.roomNodeTypes.FindIndex(x => x==roomNodeType);
                
                int newIndex = EditorGUILayout.Popup("",oldIndex, DisplayTypes());
                
                //如果从走廊更改为房间或者从房间更改为走廊
                if (newIndex != oldIndex)
                {
                    if (roomNodeTypeList.roomNodeTypes[oldIndex].isCorridor&&!roomNodeTypeList.roomNodeTypes[newIndex].isCorridor
                        ||!roomNodeTypeList.roomNodeTypes[oldIndex].isCorridor&&roomNodeTypeList.roomNodeTypes[newIndex].isCorridor)
                    {
                        ClearLinks();
                    }
                }
                
                roomNodeType = roomNodeTypeList.roomNodeTypes[newIndex];
            }
            
            if (EditorGUI.EndChangeCheck())
            {
                EditorUtility.SetDirty(this);
            }
            
            GUILayout.EndArea();
        }
        private string[] DisplayTypes()
        {
            string[] roomArray = new string[roomNodeTypeList.roomNodeTypes.Count];
            for (int i = 0; i < roomNodeTypeList.roomNodeTypes.Count; i++)
            {
                if (roomNodeTypeList.roomNodeTypes[i].displayInNodeGraphEditor)
                {
                    roomArray[i] = roomNodeTypeList.roomNodeTypes[i].roomNodeTypeName;
                }
            }

            return roomArray;
        }

        public void AddParentNode(RoomNodeSO parentRoomNodeSo)
        {
            parentRoomNodeIDList.Add(parentRoomNodeSo.id);
            GUI.changed = true;
            EditorUtility.SetDirty(this);
        }
        
        public void RemoveParentNode(RoomNodeSO parentRoomNodeSo)
        {
            if (parentRoomNodeIDList.Contains(parentRoomNodeSo.id))
            {
                parentRoomNodeIDList.Remove(parentRoomNodeSo.id);
                GUI.changed = true;
                EditorUtility.SetDirty(this);

            }
        }
        
        public void AddChildNode(RoomNodeSO childRoomNodeSo)
        {
            childRoomNodeIDList.Add(childRoomNodeSo.id);
            GUI.changed = true;
            EditorUtility.SetDirty(this);
        }
        
        public void RemoveChildNode(RoomNodeSO childRoomNodeSo)
        {
            if (childRoomNodeIDList.Contains(childRoomNodeSo.id))
            {
                childRoomNodeIDList.Remove(childRoomNodeSo.id);
                GUI.changed = true;
                EditorUtility.SetDirty(this);

            }
        }
        
        public void ClearLinks()
        {
            foreach (var id in parentRoomNodeIDList)
            {
                var parentNode = roomNodeGraph.GetRoomNodeById(id);
                // 移除父节点中记录该节点的id
                parentNode.RemoveChildNode(this);
            }
            foreach (var id in childRoomNodeIDList)
            {
                var childNode = roomNodeGraph.GetRoomNodeById(id);
                // 移除子节点中记录该节点的id
                childNode.RemoveParentNode(this);
            }
            parentRoomNodeIDList.Clear();
            childRoomNodeIDList.Clear();
            GUI.changed = true;
            EditorUtility.SetDirty(this);
        }
        
        public void ProgressEvents(Event current)
        {
            switch (current.type)
            {
                case  EventType.MouseDown:
                    ProgressMouseDown(current);
                    break;
                case EventType.MouseUp:
                    ProgressMouseUp(current);
                    break;
                case EventType.MouseDrag:
                    ProgressMouseDrag(current);
                    break;
            }
        }

        private void ProgressMouseDrag(Event current)
        {
            if (current.button == 0)
            {
                isLeftClickDragging = true;
                DragNode(current.delta);
                GUI.changed = true;
                
            }
        }

        public void DragNode(Vector2 delta)
        {
            mRect.position += delta;
            EditorUtility.SetDirty(this);
        }

        private void ProgressMouseUp(Event current)
        {
            if (current.button == 0)
            {
                isLeftClickDragging = false;
            }
        }

        private void ProgressMouseDown(Event current)
        {
            if (current.button==0)
            {
                Selection.activeObject = this;
                isSelected=!isSelected;
                
                GUI.changed = true;
            }else if (current.button==1)
            {
                //设置起始点
                roomNodeGraph.SetLineFrom(this,current.mousePosition);
            }
        }

#endif

    }
    
}
