using System;
using System.Collections;
using System.Collections.Generic;
using QFramework.Gun;
using UnityEditor;
using UnityEngine;

namespace QFramework.NodeGraph
{
    [CreateAssetMenu(fileName = "RoomNodeGraphSO_", menuName = "Scriptable Objects/Room Node Graph")]
    public class RoomNodeGraphSO : ScriptableObject
    {
        public RoomNodeTypeListSO roomNodeTypeList;
        public Dictionary<string, RoomNodeSO> roomNodeDictionary = new Dictionary<string, RoomNodeSO>();
        [HideInInspector]public List<RoomNodeSO> roomNodeList = new List<RoomNodeSO>();

        private void Awake()
        {
            LoadRoomNodeDictionary();
        }
        
#if UNITY_EDITOR
        [HideInInspector]public RoomNodeSO lineFRoomNodeSo = null;
        [HideInInspector]public Vector2 lineStartPoint = Vector2.zero;

        private void LoadRoomNodeDictionary()
        {
            roomNodeDictionary.Clear();
            foreach (var roomNodeSo in roomNodeList)
            {
                roomNodeDictionary.Add(roomNodeSo.id, roomNodeSo);
            }
        }
        public void SetLineFrom(RoomNodeSO roomNodeSo, Vector2 point)
        {
            lineFRoomNodeSo = roomNodeSo;
            lineStartPoint = point;
        }
        
        public void ClearLineFrom()
        {
            lineFRoomNodeSo = null;
            lineStartPoint = Vector2.zero;
        }
        public void AddRoomNode(RoomNodeSO roomNodeSo)
        {
            roomNodeList.Add(roomNodeSo);
            roomNodeDictionary.Add(roomNodeSo.id, roomNodeSo);
            GUI.changed = true;
            EditorUtility.SetDirty(this);
            
            AssetDatabase.AddObjectToAsset(roomNodeSo, this);
            AssetDatabase.SaveAssets();
        }
        
        public void RemoveNodeByIds(Queue<string> selectedIds)
        {
            while (selectedIds.Count>0)
            {
                var  id = selectedIds.Dequeue();
                var  roomNodeSo = roomNodeDictionary[id];
                roomNodeList.Remove(roomNodeSo);
                roomNodeDictionary.Remove(id);
                DestroyImmediate(roomNodeSo, true);
            }
            //删除节点
            GUI.changed = true;
            // 标记脏数据
            EditorUtility.SetDirty(this);
            
            AssetDatabase.SaveAssets();
        }

        public RoomNodeSO GetRoomNodeById(string id)
        {
            if (roomNodeDictionary.TryGetValue(id,out RoomNodeSO nodeSo))
            {
                return  nodeSo;
            }
            return null;
        }

        public IEnumerable<RoomNodeSO> GetChildNodes(RoomNodeSO nodeSo)
        {
            foreach (var id in nodeSo.childRoomNodeIDList)
            {
                yield return  GetRoomNodeById(id);
            }
        }
        
        public RoomNodeSO GetRoomNodeByType(RoomNodeTypeSO nodeType)
        {
            var nType = roomNodeList.Find(x => x.roomNodeType == nodeType);
            if (nType==null)
            {
                Debug.LogError($"{nodeType.name} is null");
            }
            return nType;
        }
        
        public void ClearSelectedNodes()
        {
            foreach (var roomNodeSo in roomNodeList)
            {
                roomNodeSo.isSelected = false;
            }
        }
#endif
        
        private void OnValidate()
        {
            //如果Dictionary是在ScriptableObject中作为成员变量声明，
            //并且尝试在编辑器模式下保存或加载ScriptableObject实例，Unity的序列化系统可能会导致字典的内容丢失。
            LoadRoomNodeDictionary();
#if UNITY_EDITOR
            CheckUtility.ValidateCheckNull(this, nameof(roomNodeTypeList), roomNodeTypeList);
#endif
        }
        
    }
    
}

