﻿using System;
using System.Collections.Generic;
using QFramework.Gun;
using UnityEngine;

namespace QFramework.NodeGraph
{
    [CreateAssetMenu(fileName = "RoomNodeTypeListSO_", menuName = "Scriptable Objects/Room Node Type List")]
    public class RoomNodeTypeListSO:ScriptableObject
    {
        public List<RoomNodeTypeSO> roomNodeTypes;

        private void OnValidate()
        {
#if UNITY_EDITOR
            CheckUtility.ValidateCheckEnumerableValues(this, nameof(roomNodeTypes), roomNodeTypes);
#endif
        }
    }
}