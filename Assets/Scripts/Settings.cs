using UnityEngine;

namespace  QFramework.Gun
{
    public class Settings 
    {
        public const int maxDungeonRebuildAttemptsForRoomGraph = 10;
        public const int maxDungeonBuildAttempts = 1000;
        public const float fadeInTime = 0.5f;
        #region ANIMATOR PARAMETERS
        // Animator parameters - Player
        public static int aimUp = Animator.StringToHash("aimUp");
        public static int aimDown = Animator.StringToHash("aimDown");
        public static int aimUpRight = Animator.StringToHash("aimUpRight");
        public static int aimUpLeft = Animator.StringToHash("aimUpLeft");
        public static int aimRight = Animator.StringToHash("aimRight");
        public static int aimLeft = Animator.StringToHash("aimLeft");
        public static int isIdle = Animator.StringToHash("isIdle");
        public static int isMoving = Animator.StringToHash("isMoving");
        
        public static int rollUp = Animator.StringToHash("rollUp");
        public static int rollRight = Animator.StringToHash("rollRight");
        public static int rollLeft = Animator.StringToHash("rollLeft");
        public static int rollDown = Animator.StringToHash("rollDown");
        public static int open = Animator.StringToHash("open");

        #endregion
        

        #region GAMEOBJECT TAGS
        public const string playerTag = "Player";
        public const string playerWeapon = "playerWeapon";
        #endregion
    }
    
}
