﻿using UnityEngine;

namespace QFramework.Gun
{
    public static class CheckUtility
    {

        public static bool ValidateCheckEmptyString(Object obj,string fileName, string strValue)
        {
            if (string.IsNullOrEmpty(strValue))
            {
                Debug.LogError($"{obj.name.ToString()}.{fileName} is empty");
                return  true;
            }
            return  false;
        }
        
        public  static bool ValidateCheckNull(Object obj, string fileName, Object objValue)
        {
            if (objValue == null)
            {
                Debug.LogError($"{obj.name.ToString()}.{fileName} is null");
                return true;
            }
            return false;
        }

        public static bool ValidateCheckEnumerableValues(Object obj, string fileName,
            System.Collections.IEnumerable enumerableValues)
        {
            var err = false;
            if (enumerableValues==null)
            {
                Debug.LogError($"{obj.name.ToString()}.{fileName} is null");
                err = true;
            }
            else
            {
                foreach (var enumerableValue in enumerableValues)
                {
                    if (enumerableValue == null)
                    {
                        Debug.LogError($"{obj.name.ToString()}.{fileName} has null");
                        err = true;
                        break;
                    }
                }
            }
            return err;
        }
        
        public static bool ValidateCheckNullValue(Object thisObject, string fieldName, UnityEngine.Object objectToCheck)
        {
            if (objectToCheck == null)
            {
                Debug.Log(fieldName + " is null and must contain a value in object " + thisObject.name.ToString());
                return true;
            }
            return false;
        }
        
        public static bool ValidateCheckPositiveValue(Object thisObject, string fieldName, int valueToCheck, bool isZeroAllowed)
        {
            bool error = false;

            if (isZeroAllowed)
            {
                if (valueToCheck < 0)
                {
                    Debug.Log(fieldName + " must contain a positive value or zero in object " + thisObject.name.ToString());
                    error = true;
                }
            }
            else
            {
                if (valueToCheck <= 0)
                {
                    Debug.Log(fieldName + " must contain a positive value in object " + thisObject.name.ToString());
                    error = true;
                }
            }

            return error;
        }
        
    }
}